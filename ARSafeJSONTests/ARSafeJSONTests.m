#import "Kiwi.h"
#import "ARSafeJSON.h"

SPEC_BEGIN(ARSafeJSONTests)
describe(@"ARSafeJSON", ^{
    
    context(@"When created", ^{
        
        __block ARSafeJSON *cleaner;
        
        beforeEach(^{
            cleaner = [[ARSafeJSON alloc] init];
        });
        
        it(@"is returns nil if the dictionary in argument is nil", ^{
            NSDictionary *cleanJson = [cleaner cleanUpJson:nil];
            
            [cleanJson shouldBeNil];
        });
        
        it(@"returls nil if the JSON is an NSNull", ^{
            id cleanJSON = [cleaner cleanUpJson:[NSNull null]];
            [cleanJSON shouldBeNil];
        });
        
        it(@"it doesn't delete good keys ", ^{
            NSDictionary *goodJson = [NSDictionary dictionaryWithObject:@"Hello" forKey:@"Hello"];
            NSDictionary *cleanJson = (NSDictionary *)[cleaner cleanUpJson:goodJson];
            
            [[[cleanJson objectForKey:@"Hello"] should] equal:@"Hello"];
        });
        
        it(@"it removes empty keys", ^{
            NSDictionary *dirtyJson = [NSDictionary dictionaryWithObject:@"" forKey:@"Hello"];
            NSDictionary *cleanJson = (NSDictionary *)[cleaner cleanUpJson:dirtyJson];
            
            [[cleanJson objectForKey:@"Hello"] shouldBeNil];
        });
        
        it(@"removes NSNull objects", ^{
            NSDictionary *dirtyJson = [NSDictionary dictionaryWithObject:[NSNull null] forKey:@"nullInstance"];
            NSDictionary *cleanJson = (NSDictionary *)[cleaner cleanUpJson:dirtyJson];
            
            [[cleanJson objectForKey:@"nullInstance"] shouldBeNil];
        });
        
        it(@"doesn't remove good dicos", ^{
            NSDictionary *dico = [NSDictionary dictionaryWithObjectsAndKeys:@"Holla", @"Value1", @"Hello", @"Value2", nil];
            NSDictionary *dirtyJson = [NSDictionary dictionaryWithObject:dico forKey:@"Data"];
            NSDictionary *cleanJson = (NSDictionary *)[cleaner cleanUpJson:dirtyJson];
            
            [[theValue([(NSDictionary *)[cleanJson objectForKey:@"Data"] count]) should] equal:theValue(2)];
            [[[[cleanJson objectForKey:@"Data"] objectForKey:@"Value1"] should] equal:@"Holla"];
            [[[[cleanJson objectForKey:@"Data"] objectForKey:@"Value2"] should] equal:@"Hello"];
        });
        
        it(@"removes NSNull Object in deeper dico", ^{
            NSDictionary *dico = [NSDictionary dictionaryWithObjectsAndKeys:[NSNull null], @"nullInstance", @"hello", @"Hello", nil];
            NSDictionary *dirtyJson = [NSDictionary dictionaryWithObject:dico forKey:@"Data"];
            NSDictionary *cleanJson = (NSDictionary *)[cleaner cleanUpJson:dirtyJson];
            
            NSDictionary *data = [cleanJson objectForKey:@"Data"];
            [[theValue([data count]) should] equal:theValue(1)];
            [[[cleanJson objectForKey:@"Data"] objectForKey:@"nullInstance"] shouldBeNil];
        });
        
        it(@"doesn't remove good values inside Arrays", ^{
            NSArray *array = [NSArray arrayWithObjects:@"Holla", @"Value1", @"Hello", @"Value2", nil];
            NSDictionary *dirtyJson = [NSDictionary dictionaryWithObject:array forKey:@"Data"];
            NSDictionary *cleanJson = (NSDictionary *)[cleaner cleanUpJson:dirtyJson];
            
            [[theValue([(NSArray *)[cleanJson objectForKey:@"Data"] count]) should] equal:theValue(4)];
        });
        
        it(@"removes bad values inside arrays", ^{
            NSArray *array = [NSArray arrayWithObjects:@"Holla", [NSNull null], @"Hello", @"Value2", nil];
            NSDictionary *dirtyJson = [NSDictionary dictionaryWithObject:array forKey:@"Data"];
            NSDictionary *cleanJson = (NSDictionary *)[cleaner cleanUpJson:dirtyJson];
            
            [[theValue([(NSArray *)[cleanJson objectForKey:@"Data"] count]) should] equal:theValue(3)];
        });

        
        it(@"cleans up Array type json", ^{
            NSArray *array = [NSArray arrayWithObjects:@"Holla", [NSNull null], @"Hello", @"Value2", nil];
            NSArray *cleanJson = (NSArray *)[cleaner cleanUpJson:array];
            
            [[theValue([cleanJson count]) should] equal:theValue(3)];
        });
        
        it(@"doesn't bother a good array", ^{
            NSArray *array = [NSArray arrayWithObjects:@"Holla", @"Hello Dan", @"Hello", @"Value2", nil];
            NSArray *cleanJson = (NSArray *)[cleaner cleanUpJson:array];
            
            [[theValue([cleanJson count]) should] equal:theValue(4)];
            
        });
        
        it(@"Doesn't delete an object when two objects are the same", ^{
            NSMutableDictionary *dico = [[NSMutableDictionary alloc] initWithCapacity:2];
            [dico setObject:@"hello" forKey:@"object1"];
            [dico setObject:@"hello" forKey:@"object2"];
            
            NSDictionary *json = [cleaner cleanUpJson:dico];
            
            [[theValue([json count]) should] equal:theValue([dico count])];
            
        });
        
    });
});
SPEC_END