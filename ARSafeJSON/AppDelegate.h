//
//  AppDelegate.h
//  ARSafeJSON
//
//  Created by Antoine Rabanes on 27/01/2013.
//  Copyright (c) 2013 App Revolution Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
